/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Slots,
    affects,
    definition,
    editor,
    insertVariable,
    isVariable,
    lookupVariable,
    makeMarkdownSafe,
    pgettext,
    populateVariables,
    tripetto,
} from "tripetto";
import { TMode } from "../runner/mode";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:email", "Match email address");
    },
})
export class EmailCondition extends ConditionBlock {
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    mode: TMode = "address";

    @definition
    @affects("#name")
    match?: string;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot instanceof Slots.String) {
            const wrap = (s: string) =>
                (s &&
                    (this.mode === "domain" || this.mode === "not-domain") &&
                    `_\@${s}_`) ||
                s;
            const match: string =
                (isVariable(this.match)
                    ? lookupVariable(this, this.match)?.label &&
                      `@${this.match}`
                    : this.match && wrap(makeMarkdownSafe(this.match))) ||
                "\\_\\_";

            switch (this.mode) {
                case "domain":
                    return `@${this.slot.id} ${pgettext(
                        "block:email",
                        "from domain"
                    )} ${match}`;
                case "not-domain":
                    return `@${this.slot.id} ${pgettext(
                        "block:email",
                        "not from domain"
                    )} ${match}`;
                case "address":
                    return `@${this.slot.id} = ${match}`;
                case "not-address":
                    return `@${this.slot.id} \u2260 ${match}`;
                case "defined":
                    return `@${this.slot.id} ${pgettext(
                        "block:email",
                        "not empty"
                    )}`;
                case "undefined":
                    return `@${this.slot.id} ${pgettext(
                        "block:email",
                        "empty"
                    )}`;
            }
        }

        return this.type.label;
    }

    get title() {
        return this.node?.label;
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:email", "Compare mode"),
            controls: [
                new Forms.Radiobutton<TMode>(
                    [
                        {
                            label: pgettext(
                                "block:email",
                                "Email address matches"
                            ),
                            value: "address",
                        },
                        {
                            label: pgettext(
                                "block:email",
                                "Email address does not match"
                            ),
                            value: "not-address",
                        },
                        {
                            label: pgettext(
                                "block:email",
                                "Email address matches domain"
                            ),
                            value: "domain",
                        },
                        {
                            label: pgettext(
                                "block:email",
                                "Email address does not match domain"
                            ),
                            value: "not-domain",
                        },
                        {
                            label: pgettext(
                                "block:email",
                                "Email address is not empty"
                            ),
                            value: "defined",
                        },
                        {
                            label: pgettext(
                                "block:email",
                                "Email address is empty"
                            ),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "address")
                ).on((mode: Forms.Radiobutton<TMode>) => {
                    form.visible(
                        mode.value !== "defined" && mode.value !== "undefined"
                    );

                    switch (mode.value) {
                        case "address":
                            form.title = pgettext(
                                "block:email",
                                "If email address matches"
                            );
                            break;
                        case "not-address":
                            form.title = pgettext(
                                "block:email",
                                "If email address does not match"
                            );
                            break;
                        case "domain":
                            form.title = pgettext(
                                "block:email",
                                "If email address matches domain"
                            );
                            break;
                        case "not-domain":
                            form.title = pgettext(
                                "block:email",
                                "If email address does not match domain"
                            );
                            break;
                    }

                    if (textControl.isInteractable) {
                        textControl.focus();
                    }
                }),
            ],
        });

        const isVar = (this.match && isVariable(this.match)) || false;
        const variables = populateVariables(
            this,
            (slot) =>
                slot instanceof Slots.String || slot instanceof Slots.Text,
            isVar ? this.match : undefined,
            false,
            this.slot?.id
        );
        const textControl = new Forms.Text(
            "singleline",
            !isVar ? this.match : ""
        )
            .label(pgettext("block:email", "Use fixed text"))
            .action("@", insertVariable(this, "exclude"))
            .autoFocus()
            .enter(this.editor.close)
            .escape(this.editor.close)
            .on((input) => {
                if (input.isFormVisible && input.isObservable) {
                    this.match = input.value;
                }
            });
        const variableControl = new Forms.Dropdown(
            variables,
            isVar ? this.match : ""
        )
            .label(pgettext("block:email", "Use value of"))
            .width("full")
            .on((variable) => {
                if (variable.isFormVisible && variable.isObservable) {
                    this.match = variable.value || undefined;
                }
            });

        const form = this.editor
            .form({
                title: pgettext("block:email", "If email address matches"),
                controls: [
                    new Forms.Radiobutton<"text" | "variable">(
                        [
                            {
                                label: pgettext("block:email", "Text"),
                                value: "text",
                            },
                            {
                                label: pgettext("block:email", "Value"),
                                value: "variable",
                                disabled: variables.length === 0,
                            },
                        ],
                        isVar ? "variable" : "text"
                    ).on((type) => {
                        textControl.visible(type.value === "text");
                        variableControl.visible(type.value === "variable");

                        if (textControl.isInteractable) {
                            textControl.focus();
                        }
                    }),
                    textControl,
                    variableControl,
                ],
            })
            .visible(this.mode !== "defined" && this.mode !== "undefined");
    }
}
