export type TMode =
    | "address"
    | "not-address"
    | "domain"
    | "not-domain"
    | "defined"
    | "undefined";
